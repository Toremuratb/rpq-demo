﻿function OpenCreatePopup() {
    var div = $("#DivToAppendPartialView");
    div.load("/Home/Create", function () {
        div.dialog({
            modal: true,
            width: 500,
            height: 438,
            title: "Add new customer",
            resizable: false
        });
    });
}