﻿/*
    TO DO
    DROPDOWN MENU WITH ENTITIES FROM DATABASE!!!
    OPEN POPUP MENU, WHEN SELECTED EDIT!!!
*/





/*Script for control users table.*/
$("#procurement").jqGrid({
    url: "/Procurement/GetSuppliers",
    datatype: 'json',
    mtype: 'Get',
    autowidth: true,
    shrinkToFit: true,
    colNames: ['ID', 'Created by', 'Approved by', 'NAMEOFSUPPLIER', 'SIGNEDDATE', 'DESCRIPTION', ' '],
    colModel: [
        { key: true, hidden: true, name: 'ID', index: 'ID', editable: false },
        { key: false, hidden: true, name: 'REFCREATEDBY', index: 'REFCREATEDBY', editable: true },
        { key: false, hidden: true, name: 'REFAPPROVEDBY', index: 'REFAPPROVEDBY', editable: true },
        { key: false, name: 'NAMEOFSUPPLIER', index: 'NAMEOFSUPPLIER', editable: true },
        { key: false, name: 'SIGNEDDATE', index: 'SIGNEDDATE', editable: true },
        { key: false, name: 'DESCRIPTION', index: 'DESCRIPTION', editable: true },
        {
            name: 'myac', fixed: true, sortable: false, resize: false, formatter: 'actions', formatoptions: {
                keys: false,
                editbutton: true, delbutton: true, editformbutton: false, onSuccess: function (response) {
                },
                extraparam: { oper: 'edit' },
                url: '/Pipeline/Edit',
                zIndex: 100,
                closeOnEscape: true,
                closeAfterEdit: true,
                recreateForm: true,
                afterComplete: function (response) {
                    if (response.responseText) {
                        alert(response.responseText);
                    }
                }
            }
        }],
    /*
    for edit....
    { key: false, name: 'REFPOSITION', index: '
    REFPOSITION', editable: true, edittype: 'select', editoptions: { value: { '1': '1', '2': '2', '3': '3', '4': '4' } } },
        { key: false, name: 'GENDER', index: 'GENDER', editable: true, edittype: 'select', editoptions: { value: { 'M': 'Male', 'F': 'Female', 'N': 'None' } } }],
    */


    rowNum: 10,
    rownumbers: true,
    rownumWidth: 25,
    rowList: [10, 20, 30],
    pager: jQuery('#jqControls'),
    height: '100%',
    viewrecords: true,
    caption: 'List of suppliers',
    emptyrecords: 'No suppliers information are available to display',

    jsonReader: {
        root: "rows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems: false,
        Id: "0"
    },
    multiselect: false
}).navGrid('#jqControls', { edit: true, add: true, del: false, search: false, refresh: true }, {
    zIndex: 100,
    url: '/Procurement/Edit',
    closeOnEscape: true,
    closeAfterEdit: true,
    recreateForm: true,
    afterComplete: function (response) {
        if (response.responseText) {
            alert(response.responseText);
        }
    }
}, {
        zIndex: 100,
        url: "/Procurement/Create",
        closeOnEscape: true,
        closeAfterAdd: true,
        afterComplete: function (response) {
            if (response.responseText) {
                alert(response.responseText);
            }
        }
    },
    {
        zIndex: 100,
        url: "/Procurement/Delete",
        closeOnEscape: true,
        closeAfterDelete: true,
        recreateForm: true,
        msg: "Are you sure you want to delete User...?",
        afterComplete: function (response) {
            if (response.responseText) {
                alert(response.responseText);
            }
        }
});

$('#jqGrid').navGrid('#jqGridPager', {
    width: 450,
    editCaption: "The Edit Dialog",
    recreateForm: true,
    closeAfterEdit: true,
    viewPagerButtons: false,
    afterShowForm: populateCities,
    errorTextFormat: function (data) {
        return 'Error: ' + data.responseText;
    }
},
    {
        closeAfterAdd: true,
        recreateForm: true,
        errorTextFormat: function (data) {
            return 'Error: ' + data.responseText;
        }
    }, {
        errorTextFormat: function (data) {
            return 'Error: ' + data.responseText;
        }
    });

function sizeTypes() {
    updateSizeTypeCallBack($("#Size").val(), trye);
    $("#Size").bind("change", function (e) {
        updateSizeTypeCallBack($("Size").val(), false);
    });
}

function updateSizeTypeCallBack(sizeType, setselected) {
    var current = $("#jqGrid").jqGrid('getRowData', $("crudCutomer")[0].p.selrow).REFCUSTOMERSIZE;
    var sizeTypeVal = $("#Size").val();
    $("Type").html("<option value=''>Loading cities...</option>")
        .attr("disabled", "disabled");  

    $.ajax({
        url: '/Pipeline/GetCustomerSize',
        type: "GET",
        dataType: "JSON",
        async: false,
        data: { size, sizeTypeVal },
        success: function (sizeType) {
            $("#Type").html("");
            $.each(sizeType, function (i, size) {
                $("#Type").append(
                    $('<option></option>').val(size.Type).html(size.Type));
            });
            $("#Type").prop("disabled", false);
            $("#Type").val(current);
        }
    });
}