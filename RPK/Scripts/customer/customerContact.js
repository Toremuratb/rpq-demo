﻿
/*Script for control users table.*/
$("#customerContacts").jqGrid({
    url: "/CustomerContact/GetCustomerContacts",
    datatype: 'json',
    mtype: 'Get',
    autowidth: true,
    shrinkToFit: true,
    colNames: ['ID', 'Last name', 'Middle name', 'First name', 'Phone', 'Phone 2', 'Email', 'Position', 'Ref customer', ' '],
    colModel: [
        { key: true, hidden: true, name: 'ID', index: 'ID', editable: false },
        { key: false, name: 'LNAME', index: 'LNAME', editable: true },
        { key: false, name: 'MNAME', index: 'MNAME', editable: true },
        { key: false, name: 'FNAME', index: 'FNAME', editable: false },
        { key: false, name: 'CELLPHONE1', index: 'CELLPHONE1', editable: true },
        { key: false, name: 'CELLPHONE2', index: 'CELLPHONE2', editable: true },
        { key: false, name: 'CONTACTEMAIL', index: 'CONTACTEMAIL', editable: true },
        { key: false, name: 'POSITION', index: 'POSITION', editable: true },
        { key: false, name: 'REFCUSTOMER', index: 'REFCUSTOMER', editable: true },
        {
            name: 'myac', fixed: true, sortable: false, resize: false, formatter: 'actions', formatoptions: {
                keys: false,
                editbutton: true, delbutton: true, editformbutton: false, onSuccess: function (response) {
                },
                extraparam: { oper: 'edit' },
                url: '/Pipeline/Edit',
                zIndex: 100,
                closeOnEscape: true,
                closeAfterEdit: true,
                recreateForm: true,
                afterComplete: function (response) {
                    if (response.responseText) {
                        alert(response.responseText);
                    }
                }
            }
        }],
    /*
    for edit....
    { key: false, name: 'REFPOSITION', index: 'REFPOSITION', editable: true, edittype: 'select', editoptions: { value: { '1': '1', '2': '2', '3': '3', '4': '4' } } },
        { key: false, name: 'GENDER', index: 'GENDER', editable: true, edittype: 'select', editoptions: { value: { 'M': 'Male', 'F': 'Female', 'N': 'None' } } }],
    */


    rowNum: 10,
    rownumbers: true,
    rownumWidth: 25,
    rowList: [10, 20, 30],
    pager: jQuery('#jqControls'),
    height: '100%',
    viewrecords: true,
    caption: 'List of customers',
    emptyrecords: 'No Customers information are Available to Display',
    /*
    onSelectRow: function (ids) {
        if (ids = null) {
            ids = 0;
            if (jQuery("#list10_d").jqGrid('getGridParam', 'records') > 0) {
                jQuery('#list10_d').jqGrid('setGridParam', {url:"subgrid")
            }
        }
    }
    */

    /*
   
       onSelectRow: function () {
           var rowId = $("#grid").getGridParam('selrow');
           url: '/Users/SetId?data' + rowId;
           $.post('/Users/SetId', rowId);
   
           url: "/Users/SetId"
       },
       /*
   
       //Testing call controller Users.SetId...
       onSelectRow: function (id) {
   
           //testing example 1
           var rowId = $("#grid").getGridParam('selrow');
           //end testing example 1
   
           testing example 2
           var userId;
           var rowData;
           rowData = jQuery(this).getRowData(id);
           userID = rowData['ID'];
           console.log("Users/SetId" + UserID);
           document.location.href = "Users/SetId" + UserID;
           url: "Users/SetId";
           //end testing example 2
   
           //testing example 3
           $.ajax({
               url: '@Url.Action("Details","Name")',
               data: { 'Id': id },
               success: function (detailsHtml) {
                   $('#NameDetails').html(detailsHtml);
               },
               dataType: 'html'
           });
           //end testing example 3
       },
       */
    //End testing.....

    jsonReader: {
        root: "rows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems: false,
        Id: "0"
    },
    multiselect: false
}).navGrid('#jqControls', { edit: true, add: true, del: true, search: false, refresh: true }, {
    zIndex: 100,
    url: '/CustomerContact/Edit',
    closeOnEscape: true,
    closeAfterEdit: true,
    recreateForm: true,
    afterComplete: function (response) {
        if (response.responseText) {
            alert(response.responseText);
        }
    }
}, {
        zIndex: 100,
        url: "/CustomerContact/Create",
        closeOnEscape: true,
        closeAfterAdd: true,
        afterComplete: function (response) {
            if (response.responseText) {
                alert(response.responseText);
            }
        }
    },
    {
        zIndex: 100,
        url: "/CustomerContact/Delete",
        closeOnEscape: true,
        closeAfterDelete: true,
        recreateForm: true,
        msg: "Are you sure you want to delete User...?",
        afterComplete: function (response) {
            if (response.responseText) {
                alert(response.responseText);
            }
        }
    });
