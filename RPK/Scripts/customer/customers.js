﻿/*
    TO DO
    DROPDOWN MENU WITH ENTITIES FROM DATABASE!!!
    OPEN POPUP MENU, WHEN SELECTED EDIT!!!
*/

$("#customers").jqGrid({
    url: "/Home/GetCustomers",
    datatype: 'json',
    mtype: 'Get',
    autowidth: true,
    shrinkToFit: true,
    colNames: ['ID', 'Customers', 'Total amount'],
    colModel: [
        { key: true, hidden: true, name: 'ID', index: 'ID', editable: true },
        { name: 'NAMEOFCUSTOMER', index: 'NAMEOFCUSTOMER', editable: true },
        { name: 'TOTALAMOUNT', index: 'TOTALAMOUNT', editable: true }],
    rowNum: 10,
    rownumbers: true,
    rownumWidth: 25,
    rowList: [10, 20, 30],
    pager: jQuery('#pager10'),
    viewrecords: true,
    multiselect: false,
    caption: "Customers",
    jsonReader: {
        root: "rows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems: false,
        Id: "0"
    },
    onSelectRow: function (id) {
        if (id === null || typeof id === "undefined") {
            id = 0;
            if (jQuery("#details").jqGrid('getGridParam', 'records') > 0) {
                jQuery("#details").jqGrid('setGridParam', { url: "/Home/GetProjectDetails?id=" + id, page: 1 });
                jQuery("#details").jqGrid('setCaption', "Customer details: " + id)
                    .trigger('reloadGrid');
            }
        } else {
            jQuery("#details").jqGrid('setGridParam', { url: "/Home/GetProjectDetails?id=" + id, page: 1 });
            jQuery("#details").jqGrid('setCaption', "Customer details: " + id)
                .trigger('reloadGrid');
        }
    },
    ondblClickRow: function (id) {
        $.ajax({
            url: 'Home/CustomerPartial?id=' + id
        })
        /*if (id === null || typeof id === "undefined") {
            //id = 0;
            var url = '@Url.Action("Details", "Customer")';
            url = url + '/?id=' + id;
            $("#ajaxresult").load(url);
        } else {
            var url = '@Url.Action("Details", "Customer")';
            url = url + '/?id=' + id;
            $("#ajaxresult").load(url);
        }*/
    }
}).navGrid('#pager10', { add: false, edit: false, del: false, search: false, refresh: true });
jQuery("#details").jqGrid({
    height: 150,
    url: '/Home/GetProjectDetails?id=0',
    datatype: "json",
    mtype: 'Get',
    autowidth: true,
    shrinkToFit: true,
    jsonReader: {
        root: "rows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems: false
    },
    colNames: ['ID', 'Project name', 'Description', 'Last name', 'First name', 'Type', 'Email', 'Phone', 'Phone', 'Sell-in', 'Sell-through', 'Total'],
    colModel: [
        { name: 'ID', hidden: true, index: 'ID' },
        { name: 'PROJECTNAME', index: 'PROJECTNAME' },
        { name: 'DESCRIPTION', index: 'DESCRIPTION', align: "right" },
        { name: 'LNAME', index: 'LNAME', align: "right" },
        { name: 'FNAME', index: 'FNAME', align: "right" },
        { name: 'TYPENAME_EN', index: 'TYPENAME_EN', align: "right" },
        { name: 'CONTACTEMAIL', index: 'CONTACTEMAIL', align: "right" },
        { name: 'CELLPHONE1', index: 'CELLPHONE1', align: "right" },
        { name: 'CELLPHONE2', index: 'CELLPHONE2', align: "right" },
        { name: 'SellIn', index: 'SellIn', align: "right" },
        { name: 'SellThrough', index: 'SellThrough', align: "right" },
        { name: 'TOTALAMOUNT', index: 'TOTALAMOUNT', align: "right", sortable: false, search: false }],
    rowNum: 5,
    rownumbers: true,
    rownumWidth: 25,
    rowList: [5, 10, 20],
    pager: '#pager10_d',
    sortname: 'PROJECTNAME',
    viewrecords: true,
    sortorder: "asc",
    multiselect: true,
    caption: "Customer projects"
}).navGrid('#pager10_d', { add: false, edit: false, del: false });