﻿$("#getDocument").jqGrid({
    url: "/Documents/GetDocuments",
    datatype: 'json',
    mtype: 'Get',
    colNames: ['Id', 'File name', 'File content', 'Add date', 'User'],
    colModel: [
        { key: true, hidden: true, name: 'Id', index: 'Id', editable: true },
        { key: false, name: 'FileName', index: 'FileName', editable: true },
        { key: false, name: 'FileContent', index: 'FileContent', editable: true },
        { key: false, name: 'AddDate', index: 'AddDate', editable: true },
        { key: false, name: 'RefUser', index: 'RefUser', editable: true }],

    pager: jQuery('#jqControls'),
    rowNum: 10,
    rownumbers: true,
    rownumWidth: 25,
    rowList: [10, 20, 30, 40, 50],
    height: '100%',
    viewrecords: true,
    caption: 'Documents list',
    emptyrecords: 'No documents information are Available to Display',
    jsonReader: {
        root: "rows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems: false,
        Id: "0"
    },
    autowidth: true,
    multiselect: false
}).navGrid('#jqControls', { edit: false, add: false, del: false, search: true, refresh: true });