﻿
/*Script for control users table.*/
$("#getUser").jqGrid({
    url: "/Users/GetUsers",
    datatype: 'json',
    mtype: 'Get',
    colNames: ['ID', 'FNAME', 'MNAME', 'LNAME', 'BIRTHDATE', 'REFBIRTHCITY', 'ADDRESS', 'REFCITY', 'REFCITIZENSHIP', 'STARTDATE', 'ENDDATE', 'REFPREVIOUSCV', 'URL_FB', 'URL_VK', 'URL_INSTAGRAM', 'URL_LINKEDIN', 'URL_HH', 'CV', 'ISINTERNAL', 'REFUSERROLE', 'REFEDITEDBY', 'EDITDATE', 'VERSION', 'ISACTIVE', 'REFSUPERVISOR'],
    colModel: [
        { key: true, hidden: false, name: 'ID', index: 'ID', editable: true },
        { key: false, name: 'FNAME', index: 'FNAME', editable: true },
        { key: false, name: 'MNAME', index: 'MNAME', editable: true },
        { key: false, name: 'LNAME', index: 'LNAME', editable: true },
        { key: false, name: 'BIRTHDATE', index: 'BIRTHDATE', editable: true },
        { key: false, name: 'REFBIRTHCITY', index: 'REFBIRTHCITY', editable: true },
        { key: false, name: 'ADDRESS', index: 'ADDRESS', editable: true },
        { key: false, name: 'REFCITY', index: 'REFCITY', editable: true },
        { key: false, name: 'REFCITIZENSHIP', index: 'REFCITIZENSHIP', editable: true },
        { key: false, name: 'STARTDATE', index: 'STARTDATE', editable: true },
        { key: false, name: 'ENDDATE', index: 'ENDDATE', editable: true },
        { key: false, name: 'REFPREVIOUSCV', index: 'REFPREVIOUSCV', editable: true },
        { key: false, name: 'URL_FB', index: 'URL_FB', editable: true },
        { key: false, name: 'URL_VK', index: 'URL_VK', editable: true },
        { key: false, name: 'URL_INSTAGRAM', index: 'URL_INSTAGRAM', editable: true },
        { key: false, name: 'URL_LINKEDIN', index: 'URL_LINKEDIN', editable: true },
        { key: false, name: 'URL_HH', index: 'URL_HH', editable: true },
        { key: false, name: 'CV', index: 'CV', editable: true },
        { key: false, name: 'ISINTERNAL', index: 'ISINTERNAL', editable: true },
        { key: false, name: 'REFUSERROLE', index: 'REFUSERROLE', editable: true },
        { key: false, name: 'REFEDITEDBY', index: 'REFEDITEDBY', editable: true },
        { key: false, name: 'EDITDATE', index: 'EDITDATE', editable: true },
        { key: false, name: 'VERSION', index: 'VERSION', editable: true },
        { key: false, name: 'ISACTIVE', index: 'ISACTIVE', editable: true },
        { key: false, name: 'REFSUPERVISOR', index: 'REFSUPERVISOR', editable: true }],
    /*
    for edit....
    { key: false, name: 'REFPOSITION', index: 'REFPOSITION', editable: true, edittype: 'select', editoptions: { value: { '1': '1', '2': '2', '3': '3', '4': '4' } } },
        { key: false, name: 'GENDER', index: 'GENDER', editable: true, edittype: 'select', editoptions: { value: { 'M': 'Male', 'F': 'Female', 'N': 'None' } } }],
    */
    pager: jQuery('#jqControls'),
    rowNum: 10,
    rowList: [10, 20, 30, 40, 50],
    height: '100%',
    viewrecords: true,
    caption: 'Users information',
    emptyrecords: 'No Users information are Available to Display',
    jsonReader: {
        root: "rows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems: false,
        Id: "0"
    },
    autowidth: true,
    multiselect: false
}).navGrid('#jqControls', { edit: true, add: true, del: true, search: false, refresh: true }, {
    zIndex: 100,
    url: '/Users/Edit',
    closeOnEscape: true,
    closeAfterEdit: true,
    recreateForm: true,
    afterComplete: function (response) {
        if (response.responseText) {
            alert(response.responseText);
        }
    }
}, {
        zIndex: 100,
        url: "/Users/Create",
        closeOnEscape: true,
        closeAfterAdd: true,
        afterComplete: function (response) {
            if (response.responseText) {
                alert(response.responseText);
            }
        }
    },
    {
        zIndex: 100,
        url: "/Users/Delete",
        closeOnEscape: true,
        closeAfterDelete: true,
        recreateForm: true,
        msg: "Are you sure you want to delete User...?",
        afterComplete: function (response) {
            if (response.responseText) {
                alert(response.responseText);
            }
        }
    });