﻿(function (mapOptions, mapContainer) {

    var mapObject = new google.maps.Map(mapContainer, mapOptions),
        route = null;


    function renderRoute(from, to) {
        var directionsService = new google.maps.DirectionsService(),
            directionsRequest = {
                origin: from,
                destination: to,
                travelMode: google.maps.DirectionsTravelMode.WALKING,
                unitSystem: google.maps.UnitSystem.METRIC
            };

        directionsService.route(directionsRequest, function (response, status) {
            if (status !== google.maps.DirectionsStatus.OK) {
                return $("#error").append("Unable to retrieve your route<br />");
            }

            route && route.setMap(null);

            route = new google.maps.DirectionsRenderer({
                map: mapObject,
                directions: response
            });
        });
    }


    $("#Taxi").click(function (event) {
        renderRoute(
            {lat: 55.721838, lng: 37.595443},
            {lat: 55.901564, lng: 37.729379}
        );
    });

    $("#Shuttle").click(function (event) {
        renderRoute(
            {lat: 55.840616, lng: 37.448766},
            {lat: 55.573020, lng: 37.626871}
        );
    });

})({
    zoom: 16,
    center: new google.maps.LatLng(53.8374829, 21.1518056),
    gestureHandling: 'cooperative',
    scrollwheel: false,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    zoomControl: true
}, document.getElementById("map"));