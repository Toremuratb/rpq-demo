﻿$("#getUser").jqGrid({
    url: "/Users/GetUsers",
    datatype: 'json',
    mtype: 'Get',
    colNames: ['ID', 'LNAME', 'FNAME', 'MNAME', 'E_MAIL', 'CELL_PHONE', 'REFPOSITION', 'GENDER'],
    colModel: [
        { key: true, hidden: true, name: 'ID', index: 'ID', editable: true },
        { key: false, name: 'LNAME', index: 'LNAME', editable: true },
        { key: false, name: 'FNAME', index: 'FNAME', editable: true },
        { key: false, name: 'MNAME', index: 'MNAME', editable: true },
        { key: false, name: 'E_MAIL', index: 'E_MAIL', editable: true },
        { key: false, name: 'CELL_PHONE', index: 'CELL_PHONE', editable: true, },
        { key: false, name: 'REFPOSITION', index: 'REFPOSITION', editable: true, edittype: 'select', editoptions: { value: { '1': '1', '2': '2', '3': '3', '4': '4' } } },
        { key: false, name: 'GENDER', index: 'GENDER', editable: true, edittype: 'select', editoptions: { value: { 'M': 'Male', 'F': 'Female', 'N': 'None' } } }],

    pager: jQuery('#jqControls'),
    rowNum: 10,
    rowList: [10, 20, 30, 40, 50],
    height: '100%',
    viewrecords: true,
    caption: 'Users information',
    emptyrecords: 'No Users information are Available to Display',
    jsonReader: {
        root: "rows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems: false,
        Id: "0"
    },
    autowidth: true,
    multiselect: false
}).navGrid('#jqControls', { edit: true, add: true, del: true, search: false, refresh: true }, {
    zIndex: 100,
    url: '/Users/Edit',
    closeOnEscape: true,
    closeAfterEdit: true,
    recreateForm: true,
    afterComplete: function (response) {
        if (response.responseText) {
            alert(response.responseText);
        }
    }
},  {
        zIndex: 100,
        url: "/Users/Create",
        closeOnEscape: true,
        closeAfterAdd: true,
        afterComplete: function (response) {
            if (response.responseText) {
                alert(response.responseText);
            }
        }
    },
    {
        zIndex: 100,
        url: "/Users/Delete",
        closeOnEscape: true,
        closeAfterDelete: true,
        recreateForm: true,
        msg: "Are you sure you want to delete User...?",
        afterComplete: function (response) {
            if (response.responseText) {
                alert(response.responseText);
            }
        }
    });