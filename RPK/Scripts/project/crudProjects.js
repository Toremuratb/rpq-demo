﻿$("#crudProjects").jqGrid({
    url: "/Project/GetProjects",
    datatype: 'json',
    mtype: 'Get',
    autowidth: true,
    shrinkToFit: true,
    colNames: ['ID', 'ISSUBSCRIPTION', 'MONTHLYPAYMENT', 'REFHISTORYPROJECTS', 'TOTALAMOUNT', 'ISPAID', 'EXPIRYDATE', 'SIGNEDDATE', 'REFMONTHS', 'FISCALYEARS', 'QUARTERS', 'SellIn', 'SellThrough'],
    colModel: [
        { key: true, hidden: true, name: 'ID', index: 'ID', editable: false },
        { key: false, name: 'ISSUBSCRIPTION', index: 'ISSUBSCRIPTION', editable: true},
        { key: false, name: 'MONTHLYPAYMENT', index: 'MONTHLYPAYMENT', editable: false },
        { key: false, name: 'REFHISTORYPROJECTS', index: 'REFHISTORYPROJECTS', editable: true },
        { key: false, name: 'TOTALAMOUNT', index: 'TOTALAMOUNT', editable: true },
        { key: false, name: 'ISPAID', index: 'ISPAID', editable: true },
        { key: false, name: 'EXPIRYDATE', index: 'EXPIRYDATE', editable: true },
        { key: false, name: 'SIGNEDDATE', index: 'SIGNEDDATE', editable: true },
        { key: false, name: 'REFMONTHS', index: 'REFMONTHS', editable: true },
        { key: false, name: 'FISCALYEARS', index: 'FISCALYEARS', editable: true },
        { key: false, name: 'QUARTERS', index: 'QUARTERS', editable: true },
        { key: false, name: 'SellIn', index: 'SellIn', editable: true },
        { key: false, name: 'SellThrough', index: 'SellThrough', editable: true }],
    editurl: 'clientArray',
    sortname: 'Name',
    height: '100%',
    viewrecords: true,
    rownumbers: true,
    sortorder: "desc",
    rowNum: 10,
    rownumbers: true,
    rownumWidth: 25,
    rowList: [10, 20, 30],
    pager: '#pager',
    caption: 'List of projects',
    emptyrecords: 'No projects...',
    jsonReader: {
        root: "rows",
        page: "page",
        total: "total",
        records: "records",
        repeatitems: false,
        Id: "0"
    },
    multiselect: false
}).jqGrid('navGrid', '#pager', { edit: true, add: true, del: false, search: false, refresh: false },
    {
        recreateForm: true,
        onClose: function () {
            resetMonth();
        }
    }, {
        recreateForm: true,
        onClose: function () {
            resetMonth();
        }
    });