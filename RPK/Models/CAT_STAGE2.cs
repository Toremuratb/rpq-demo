//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RPK.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CAT_STAGE2
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CAT_STAGE2()
        {
            this.HISTORYNEWSUPPLIERS = new HashSet<HISTORYNEWSUPPLIER>();
            this.RECORDS_REQUESTSPO_CAT_STAGE = new HashSet<RECORDS_REQUESTSPO_CAT_STAGE>();
            this.RECORDS_REQUESTSPO_STAGE = new HashSet<RECORDS_REQUESTSPO_STAGE>();
        }
    
        public int ID { get; set; }
        public string STATUS { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HISTORYNEWSUPPLIER> HISTORYNEWSUPPLIERS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RECORDS_REQUESTSPO_CAT_STAGE> RECORDS_REQUESTSPO_CAT_STAGE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RECORDS_REQUESTSPO_STAGE> RECORDS_REQUESTSPO_STAGE { get; set; }
    }
}
