//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RPK.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class WORKHISTORY
    {
        public int ID { get; set; }
        public Nullable<int> REFUSER { get; set; }
        public string WORKPLACE { get; set; }
        public Nullable<System.DateTime> STARTDATE { get; set; }
        public Nullable<System.DateTime> ENDDATE { get; set; }
        public string POSITIONNAME { get; set; }
        public string DESCRIPTION { get; set; }
        public string REFERENCES { get; set; }
        public Nullable<int> VERSION { get; set; }
        public Nullable<bool> ISACTIVE { get; set; }
    }
}
