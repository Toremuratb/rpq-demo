//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RPK.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CAT_CUSTOMERSIZE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CAT_CUSTOMERSIZE()
        {
            this.CUSTOMERs = new HashSet<CUSTOMER>();
        }
    
        public string ID { get; set; }
        public string NAME_EN { get; set; }
        public string NAME_KZ { get; set; }
        public string NAME_RU { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CUSTOMER> CUSTOMERs { get; set; }
    }
}
