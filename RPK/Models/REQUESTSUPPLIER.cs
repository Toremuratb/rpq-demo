//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RPK.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class REQUESTSUPPLIER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public REQUESTSUPPLIER()
        {
            this.HISTORYNEWSUPPLIERS = new HashSet<HISTORYNEWSUPPLIER>();
        }
    
        public int ID { get; set; }
        public Nullable<int> REFUSERS { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HISTORYNEWSUPPLIER> HISTORYNEWSUPPLIERS { get; set; }
    }
}
