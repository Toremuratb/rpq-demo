﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using RPK.Web.Models;

namespace RPK.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Shuttles ()
        {
            return View();
        }
        public ActionResult Routes()
        {
            return View();
        }
        public ActionResult Analytics()
        {
            return View();
        }
        public ActionResult Profile()
        {
            return View();
        }

        //Get Customers method... Cannot access to views...
        //ToDo access to views
        /*
        public JsonResult GetCustomers(string sidx, string sort, int page, int rows)
        {
            RpkDBEntities db = new RpkDBEntities();
            sort = (sort == null) ? "" : sort;
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            var CustomersList = db.(
        } 
        */
    }
}