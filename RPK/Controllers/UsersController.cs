﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RPK.Web.Models;
using System.Data.Entity;

namespace RPK.Web.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        public ActionResult Index()
        {
            return View();
        }

        //public method to get users... And CRUD operations...
        public JsonResult GetUsers(string sidx, string sort, int page, int rows)
        {
            RpkDBEntities db = new RpkDBEntities();
            sort = (sort == null) ? "" : sort;
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var UserList = db.USERS.Select(
                t => new
                {
                    t.ID,
                    t.FNAME,
                    t.MNAME,
                    t.LNAME,
                    t.BIRTHDATE,
                    t.REFBIRTHCITY,
                    t.ADDRESS,
                    t.REFCITY,
                    t.REFCITIZENSHIP,
                    t.STARTDATE,
                    t.ENDDATE,
                    t.REFPREVIOUSCV,
                    t.URL_FB,
                    t.URL_VK,
                    t.URL_INSTAGRAM,
                    t.URL_LINKEDIN,
                    t.URL_HH,
                    t.CV,
                    t.ISINTERNAL,
                    t.REFUSERROLE,
                    t.REFEDITEDBY,
                    t.EDITDATE,
                    t.VERSION,
                    t.ISACTIVE,
                    t.REFSUPERVISOR
                });
            int totalRecords = UserList.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            if (sort.ToUpper() == "DESC")
            {
                UserList = UserList.OrderByDescending(t => t.FNAME);
                UserList = UserList.Skip(pageIndex * pageSize).Take(pageSize);
            }
            else
            {
                UserList = UserList.OrderBy(t => t.FNAME);
                UserList = UserList.Skip(pageIndex * pageSize).Take(pageSize);
            }
            var jsonData = new
            {
                total = totalPages,
                page,
                record = totalRecords,
                rows = UserList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string Create([Bind(Exclude = "Id")] USER Model)
        {
            RpkDBEntities db = new RpkDBEntities();
            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    /* Just test!!!
                     * Guid.NewGuid() = 128bit, Long or int only 64bit...*/
                    byte[] gb = Guid.NewGuid().ToByteArray();
                    int i = BitConverter.ToInt32(gb, 0);
                    long l = BitConverter.ToInt64(gb, 0);
                    Model.ID = i;
                    /*---------------------------------------------------*/
                    db.USERS.Add(Model);
                    db.SaveChanges();
                    msg = "Saved Successfully";
                }
                else
                {
                    msg = "Validation data not successfully";
                }
            }
            catch (Exception e)
            {
                msg = "Error occured:" + e.Message;
            }
            return msg;
        }

        public string Edit(USER Model)
        {
            RpkDBEntities db = new RpkDBEntities();
            string msg;
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(Model).State = EntityState.Modified;
                    db.SaveChanges();
                    msg = "Saved successfully";
                }
                else
                {
                    msg = "Validation data not successfully";
                }
            }
            catch (Exception e)
            {
                msg = "Error occured:" + e.Message;
            }
            return msg;
        }

        public string Delete(string Id)
        {
            RpkDBEntities db = new RpkDBEntities();
            USER user = db.USERS.Find(Id);
            db.USERS.Remove(user);
            db.SaveChanges();
            return "Deleted successfully";
        }
    }
}